# Git Crash Course

  1. [Iniciándose con Git](git-1.html)
      1. A necesidade de versionar. Que é un SCM. Que é Git e por que é importante saber de Git.
      2. Instalación de Git (comando _git_). Instalación dunha interfaz gráfica. Creación dunha conta en Github.
      3. Creación dun repositorio (git __init__). Clonación dun repositorio (git __clone__).
      4. Historial de cambios (git __log__). Movéndose entre versións (git __checkout__).
      5. Área de traballo (git __status__). Paquete de cambios (git __commit__).
      6. Revisión de cambios (git __diff__).
      7. Presentación de repositorios en Github. Formato de texto Markdown e _ficheiros README_.
  2. Compartindo proxectos
      1. Identidade (git __config user__) e autenticación contra servidor remoto.
      2. Recibir cambios remotos (git __pull__). Enviar cambios a remotos (git __push__).
      3. Acceso a repositorios remotos (git __remote__). Descargar cambios (git __fetch__).
      4. Organizándonos con etiquetas (git __tag__) e pólas (git __branch__).
      5. Mistura de cambios (git __merge__) e recolocación (git __rebase__).
  3. Git e Github avanzados
      1. Proxectos derivados (forks). Pull-request e xestión de cambios.
      2. Uso de varios repositorios remotos. Submódulos (git __submodule__).
      3. Provisión de cambios (git __stash__) e apaño de cereixas (git __cherry-pick__).
      4. Auditoría de cambios (git __blame__) e refactorización de cambios (git __rebase --interactive__)
      5. Xestión de grupos en Github. Webhooks e integración de servizos en Github.
      6. Wiki dentro de Github. Github.io e publicación de páxinas con Jekyll.